# Example 4: webclient

Un simple ejemplo de como acceder al DOM desde Rust usando webplatform.

# Instrucciones

```bash
$ cargo build --target=asmjs-unknown-emscripten
$ firefox index.html
```
